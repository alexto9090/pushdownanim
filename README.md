# **「 Push Down Animation Click 」**


#### A library for Android developers who want to create "push down animation click" for view like spotify application. :)


## 「 DEMO APPLICATION 」

```
clone repo and build it :)
```

## 「 Installation 」

Gradle
```gradle
implementation 'com.gitlab.alexto9090:pushdownanim:1.2'
```

## 「 Usage 」

```java
Button button = findViewById( R.id.button );

PushDownAnim.setPushDownAnimTo( button, ... )
        .setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick( View view ){
                Toast.makeText( MainActivity.this, "PUSH DOWN !!", Toast.LENGTH_SHORT ).show();
            }

        } );

```

#### Full option

```java
PushDownAnim.setPushDownAnimTo( button, ... )
        .setScale( MODE_SCALE | MODE_STATIC_DP,
         	   PushDownAnim.DEFAULT_PUSH_SCALE | PushDownAnim.DEFAULT_PUSH_STATIC  ) 
        .setDurationPush( PushDownAnim.DEFAULT_PUSH_DURATION )
        .setDurationRelease( PushDownAnim.DEFAULT_RELEASE_DURATION )
        .setInterpolatorPush( PushDownAnim.DEFAULT_INTERPOLATOR )
        .setInterpolatorRelease( PushDownAnim.DEFAULT_INTERPOLATOR )
        .setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick( View view ){
                Toast.makeText( MainActivity.this, "PUSH DOWN !!", Toast.LENGTH_SHORT ).show();
            }
        } )
        .setOnLongClickListener( new View.OnLongClickListener(){
            @Override
            public boolean onLongClick( View view ){
		Toast.makeText( MainActivity.this, "LONG PUSH DOWN !!", Toast.LENGTH_SHORT ).show();
                return true; // true: not effect to single click
            }
        } )
        .setOnTouchEvent( new View.OnTouchListener(){
            @Override
            public boolean onTouch( View view, MotionEvent motionEvent ){
                return false;
            }
        } );

```

#### Default values

- Default mode: `MODE_SCALE`
- Default scale: `0.97f`
- Default push duration: `50 millisecond`
- Default release duration:  `125 millisecond`
- Default interpolator push/release: `AccelerateDecelerateInterpolator()`


## 「 MODE 」

**1. `MODE_SCALE`**: use range scale **0.00 - 1.00** to push down view.

>**EXAMPLE:** This case 8dp ≈ 0.89 scale

```java
PushDownAnim.setPushDownAnimTo( button )
	.setScale( MODE_SCALE, 0.89f  ) 
        ...;
```

**2. `MODE_STATIC_DP`**: use static dp unit to push down view.

>**EXAMPLE:** 8dp static scale


![TheKhaeng](./pictures/push_down_static.gif)

```java
PushDownAnim.setPushDownAnimTo( button )
	.setScale( MODE_STATIC_DP, 8  ) 
        ...;
```




## 「 Long Click 」

`setOnClickListener(...)`

```java
@Override
public boolean onLongClick( View view ){
    ...
    return true; // true: not effect to single click
}
```

>NOTE: If you do not want to trigger a normal click **`return true`**

## 「 Disable Click Programmatically 」

If your project has logic that want to disable click programmatically. You must use **``setEnabled(...)``** (do not use **`setClickable(...)`** it is not effective.)

```java
button.setEnabled( false );
```

